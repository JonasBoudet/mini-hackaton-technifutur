import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {QuestionnaireComponent} from './ComponentFront/questionnaire/questionnaire.component';
import {MonProfilComponent} from './ComponentFront/mon-profil/mon-profil.component';
import {DashboardComponent} from './ComponentFront/dashboard/dashboard.component';

const routes: Routes = [
  {path : 'questionnaire', component: QuestionnaireComponent},
  {path : 'profil', component: MonProfilComponent},
  {path : 'dashboard', component: DashboardComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
